package com.metraplasa.microservice.frontend.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.metraplasa.microservice.frontend.dto.Product;

@Component
public class CatalogServiceFallback implements CatalogService{

	@Override
	public Iterable<Product> dataProduct() {
		return new ArrayList<Product>();
	}

	@Override
	public Map<String, Object> backendInfo() {
		Map<String, Object> info = new HashMap<String, Object>();
		info.put("host", "127.0.0.1");
		info.put("address", "localhost");
		info.put("port", "8080");
		return info;
	}

}
