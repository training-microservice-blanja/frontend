package com.metraplasa.microservice.frontend.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;

import com.metraplasa.microservice.frontend.service.CatalogService;

@Controller
public class ProductController {

	@Autowired
	private CatalogService catalogService;
	
	@GetMapping("/product/list")
	public ModelMap daftarProduk() {
		return new ModelMap()
			.addAttribute("productData", catalogService.dataProduct());
	}
}
