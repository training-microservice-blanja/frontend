package com.metraplasa.microservice.frontend.dto;

import java.math.BigDecimal;

import lombok.Data;

@Data
public class Product {

	private String id;
	private String code;
	private String name;
	private BigDecimal price;
}
